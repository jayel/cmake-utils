# Finds the International Components for Unicode (ICU) Library
#
#  ICU_FOUND          - True if ICU found.
#  ICU_INCLUDE_DIRS   - Directory to include to get ICU headers
#                       Note: always include ICU headers as, e.g.,
#                       unicode/utypes.h
#  ICU_LIBRARIES      - Libraries to link against for the common ICU


if(NOT DEFINED ICU_STATIC)
    if(UNIX)
        set(ICU_STATIC ON)
    elseif(WIN32)
        set(ICU_STATIC OFF)
    endif()
endif()
set(ICU_USE_STATIC_LIBS ${ICU_STATIC})

# Look for the header file.
find_path(
  ICU_INCLUDE_DIR
  NAMES unicode/utypes.h
  DOC "Include directory for the ICU library")
mark_as_advanced(ICU_INCLUDE_DIR)

# Look for the libraries
set(__ICU_LIBRARY_NAMES
    I18N
    IO
    LE
    COMMON
    DATA
)
mark_as_advanced(__ICU_LIBRARY_NAMES)
set(__ICU_COMMON_LIB_FILES icuuc cygicuuc cygicuuc32)
mark_as_advanced(__ICU_COMMON_LIB_FILES)
set(__ICU_I18N_LIB_FILES icui18n icuin cygicuic cygicuin32)
mark_as_advanced(__ICU_I18N_LIB_FILES)
set(__ICU_DATA_LIB_FILES icudata icudt)
mark_as_advanced(__ICU_DATA_LIB_FILES)
set(__ICU_IO_LIB_FILES icuio)
mark_as_advanced(__ICU_IO_LIB_FILES)
set(__ICU_LE_LIB_FILES icule)
mark_as_advanced(__ICU_LE_LIB_FILES)

message(STATUS "ICU Static Linking: ${ICU_USE_STATIC_LIBS}")
if(ICU_USE_STATIC_LIBS)
    set(_findicu_orig_CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES})
    mark_as_advanced(_findicu_orig_CMAKE_FIND_LIBRARY_SUFFIXES)
    if(UNIX)
        set(CMAKE_FIND_LIBRARY_SUFFIXES .a)
    else()
        message(FATAL_ERROR "Static linking of ICU on non-Linux OS not supported yet")
    endif()
endif()
foreach(lib_name ${__ICU_LIBRARY_NAMES})
    find_library(
        ICU_${lib_name}_LIBRARY
        NAMES ${__ICU_${lib_name}_LIB_FILES}
        DOC "Library files")
    if(ICU_${lib_name}_LIBRARY)
        list(APPEND ICU_LIBS_FOUND ICU_${lib_name}_LIBRARY)
    endif()
endforeach(lib_name)
if(ICU_USE_STATIC_LIBS)
    set(CMAKE_FIND_LIBRARY_SUFFIXES ${_findicu_orig_CMAKE_FIND_LIBRARY_SUFFIXES})
endif()

if(ICU_INCLUDE_DIR AND ICU_LIBS_FOUND)
    set(ICU_VERSION 0)
    set(ICU_MAJOR_VERSION 0)
    set(ICU_MINOR_VERSION 0)
    if (EXISTS "${ICU_INCLUDE_DIR}/unicode/uvernum.h")
        FILE(READ "${ICU_INCLUDE_DIR}/unicode/uvernum.h" _ICU_VERSION_CONENTS)
    else()
        FILE(READ "${ICU_INCLUDE_DIR}/unicode/uversion.h" _ICU_VERSION_CONENTS)
    endif()
    string(REGEX REPLACE ".*#define U_ICU_VERSION_MAJOR_NUM ([0-9]+).*" "\\1" ICU_MAJOR_VERSION "${_ICU_VERSION_CONENTS}")
    string(REGEX REPLACE ".*#define U_ICU_VERSION_MINOR_NUM ([0-9]+).*" "\\1" ICU_MINOR_VERSION "${_ICU_VERSION_CONENTS}")
    set(ICU_VERSION "${ICU_MAJOR_VERSION}.${ICU_MINOR_VERSION}")
    if(ICU_FIND_VERSION)
        if(ICU_VERSION VERSION_LESS ICU_FIND_VERSION)
            message(SEND_ERROR "Detected version of ICU is too old: ${ICU_VERSION}")
        endif()
    endif()
    set(ICU_FOUND 1)
    set(ICU_INCLUDE_DIRS ${ICU_INCLUDE_DIR})
    foreach(lib_name ${ICU_LIBS_FOUND})
        list(APPEND ICU_LIBRARIES_INTERNAL ${${lib_name}})
    endforeach()
endif()

if(ICU_FOUND)
    message(STATUS "Found ICU Version: ${ICU_VERSION}")
    message(STATUS "Found ICU header files in ${ICU_INCLUDE_DIRS}")
    message(STATUS "Found ICU libraries: ${ICU_LIBRARIES_INTERNAL}")
    if(UNIX)
        set(ICU_LIBRARIES dl ${ICU_LIBRARIES_INTERNAL} dl ${ICU_LIBRARIES_INTERNAL} dl)
    elseif(WIN32)
        set(ICU_LIBRARIES ${ICU_LIBRARIES_INTERNAL})
    endif()
else()
    message(SEND_ERROR "Could NOT find ICU")
endif()

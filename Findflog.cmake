# - Find the curses include file and library
#
#  flog_FOUND - system has Crypto++
#  flog_INCLUDE_DIR - the Crypto++ include directory
#  flog_LIBRARIES - The libraries needed to use Crypto++
#

#=============================================================================
# Copyright 2001-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

if(NOT DEFINED flog_STATIC)
    set(flog_STATIC ON)
endif()
set(flog_old_LIBRARY_SUFFIX ${CMAKE_FIND_LIBRARY_SUFFIXES})
if(flog_STATIC)
    set(CMAKE_FIND_LIBRARY_SUFFIXES .a)
endif()
find_library(flog_flog_LIBRARY NAMES flog )
find_file(flog_HAVE_flog_H flog.hpp HINTS /usr/local/include/flog/)
find_path(flog_flog_H_PATH flog.hpp HINTS /usr/local/include/flog/)
get_filename_component(_flogLibDir "${flog_flog_LIBRARY}" PATH)
get_filename_component(_flogParentDir "${_flogLibDir}" PATH)

#file(STRINGS ${flog_HAVE_flog_H} _flog_VERSION REGEX "Crypto\\+\\+ Library [0-9\\.]+ API Reference")
#string(REGEX REPLACE "^.+ Library ([0-9\\.]+) API Reference" "\\1" flog_VERSION "${_flog_VERSION}")
message(STATUS "flog library found")

# for compatibility with older FindCurses.cmake this has to be in the cache
# FORCE must not be used since this would break builds which preload a cache wqith these variables set
set(flog_INCLUDE_PATH "${flog_flog_H_PATH}" CACHE FILEPATH "The flog include path")
set(flog_LIBRARY "${flog_flog_LIBRARY}" CACHE FILEPATH "The flog library")

# Need to provide the *_LIBRARIES
set(flog_LIBRARIES ${flog_LIBRARY} rt)

# Proper name is *_INCLUDE_DIR
set(flog_INCLUDE_DIR ${flog_INCLUDE_PATH})

mark_as_advanced(
  flog_INCLUDE_PATH
  flog_LIBRARY
  flog_flog_INCLUDE_PATH
  flog_flog_LIBRARY
  flog_LIBRARIES
  flog_INCLUDE_DIR
  flog_old_LIBRARY_SUFFIX
  )
set(CMAKE_FIND_LIBRARY_SUFFIXES ${flog_old_LIBRARY_SUFFIX})

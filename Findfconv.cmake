# - Find the curses include file and library
#
#  fconv_FOUND - system has Crypto++
#  fconv_INCLUDE_DIR - the Crypto++ include directory
#  fconv_LIBRARIES - The libraries needed to use Crypto++
#

#=============================================================================
# Copyright 2001-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

if(NOT DEFINED fconv_STATIC)
    set(fconv_STATIC ON)
endif()
set(fconv_old_LIBRARY_SUFFIX ${CMAKE_FIND_LIBRARY_SUFFIXES})
if(fconv_STATIC)
    set(CMAKE_FIND_LIBRARY_SUFFIXES .a)
endif()
find_library(fconv_fconv_LIBRARY NAMES fconv )
find_file(fconv_HAVE_fconv_H fconv.hpp HINTS /usr/local/include/fconv/)
find_path(fconv_fconv_H_PATH fconv.hpp HINTS /usr/local/include/fconv/)
get_filename_component(_fconvLibDir "${fconv_fconv_LIBRARY}" PATH)
get_filename_component(_fconvParentDir "${_fconvLibDir}" PATH)

#file(STRINGS ${fconv_HAVE_fconv_H} _fconv_VERSION REGEX "Crypto\\+\\+ Library [0-9\\.]+ API Reference")
#string(REGEX REPLACE "^.+ Library ([0-9\\.]+) API Reference" "\\1" fconv_VERSION "${_fconv_VERSION}")
message(STATUS "fconv library found")

# for compatibility with older FindCurses.cmake this has to be in the cache
# FORCE must not be used since this would break builds which preload a cache wqith these variables set
set(fconv_INCLUDE_PATH "${fconv_fconv_H_PATH}" CACHE FILEPATH "The fconv include path")
set(fconv_LIBRARY "${fconv_fconv_LIBRARY}" CACHE FILEPATH "The fconv library")

# Need to provide the *_LIBRARIES
set(fconv_LIBRARIES ${fconv_LIBRARY})

# Proper name is *_INCLUDE_DIR
set(fconv_INCLUDE_DIR ${fconv_INCLUDE_PATH})

mark_as_advanced(
  fconv_INCLUDE_PATH
  fconv_LIBRARY
  fconv_fconv_INCLUDE_PATH
  fconv_fconv_LIBRARY
  fconv_LIBRARIES
  fconv_INCLUDE_DIR
  fconv_old_LIBRARY_SUFFIX
  )
set(CMAKE_FIND_LIBRARY_SUFFIXES ${fconv_old_LIBRARY_SUFFIX})

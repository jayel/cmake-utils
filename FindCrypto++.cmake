# - Find the curses include file and library
#
#  CRYPTOPP_FOUND - system has Crypto++
#  CRYPTOPP_INCLUDE_DIR - the Crypto++ include directory
#  CRYPTOPP_LIBRARIES - The libraries needed to use Crypto++
#

#=============================================================================
# Copyright 2001-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

if(NOT DEFINED CRYPTOPP_STATIC)
    set(CRYPTOPP_STATIC OFF)
endif()
if(CRYPTOPP_STATIC)
    set(CMAKE_FIND_LIBRARY_SUFFIXES .a)
endif()
find_library(CRYPTOPP_CRYPTOPP_LIBRARY NAMES cryptopp )
find_file(CRYPTOPP_HAVE_CRYPTOPP_H cryptlib.h HINTS /usr/local/include/cryptopp/)
find_path(CRYPTOPP_CRYPTOPP_H_PATH cryptlib.h HINTS /usr/local/include/cryptopp/)
get_filename_component(_cryptoppLibDir "${CRYPTOPP_CRYPTOPP_LIBRARY}" PATH)
get_filename_component(_cryptoppParentDir "${_cryptoppLibDir}" PATH)

file(STRINGS ${CRYPTOPP_HAVE_CRYPTOPP_H} _cryptopp_VERSION REGEX "Crypto\\+\\+ Library [0-9\\.]+ API Reference")
string(REGEX REPLACE "^.+ Library ([0-9\\.]+) API Reference" "\\1" CRYPTOPP_VERSION "${_cryptopp_VERSION}")
message(STATUS "Crypto++ version: ${CRYPTOPP_VERSION}")
message(STATUS "Crypto++ library: ${CRYPTOPP_CRYPTOPP_LIBRARY}")


# for compatibility with older FindCurses.cmake this has to be in the cache
# FORCE must not be used since this would break builds which preload a cache wqith these variables set
set(CRYPTOPP_INCLUDE_PATH "${CRYPTOPP_CRYPTOPP_H_PATH}" CACHE FILEPATH "The Crypto++ include path")
set(CRYPTOPP_LIBRARY "${CRYPTOPP_CRYPTOPP_LIBRARY}" CACHE FILEPATH "The Crypto++ library")

# Need to provide the *_LIBRARIES
set(CRYPTOPP_LIBRARIES ${CRYPTOPP_LIBRARY})

# Proper name is *_INCLUDE_DIR
set(CRYPTOPP_INCLUDE_DIR ${CRYPTOPP_INCLUDE_PATH})

mark_as_advanced(
  CRYPTOPP_INCLUDE_PATH
  CRYPTOPP_LIBRARY
  CRYPTOPP_CRYPTOPP_INCLUDE_PATH
  CRYPTOPP_CRYPTOPP_LIBRARY
  CRYPTOPP_LIBRARIES
  CRYPTOPP_INCLUDE_DIR
  )


